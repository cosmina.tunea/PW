import CreateAcc from "./jsx/CreateAcc";
import Login from './jsx/Login'
import Start from './jsx/Start'
import Home from './jsx/Home'
import Conts from './jsx/Conts'
import Termeni from './jsx/Termeni';
import Profile from "./jsx/Profile";

import {
    createBrowserRouter,
    RouterProvider,
  } from "react-router-dom";
import AdminPage from "./jsx/AdminPage";

  const router = createBrowserRouter([
    {
      path: "/",
      element: <Start/>,
    },
    {
        path: '/register',
        element: <CreateAcc/>
    },
    {
        path: '/login',
        element: <Login/>
    },
    {
      path: '/home',
      element: <Home/>
    },
    {
      path: '/profile',
      element: <Profile/>
    },
    {
      path: '/admin',
      element: <AdminPage/>
    },
    {
      path: '/termeni',
      element: <Termeni/>
    },
    {
      path: '/conts',
      element: <Conts/>
    }
  ]);

export default function App(){
    return (
        <RouterProvider router={router} />
    )
}