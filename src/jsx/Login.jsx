import '../css/Login.css'
import { Link, useNavigate} from 'react-router-dom'
import { getAuth, onAuthStateChanged, signInWithEmailAndPassword} from 'firebase/auth'
import { useEffect, useState } from 'react'
import { collection, getDocs, query, where } from 'firebase/firestore';
import { db } from './firebase';

function Login() {
    const [email, setEmail] = useState('');
    const [password,setPassword] = useState('');


    const [user,setUser] = useState(null);
    const auth = getAuth();

    const navigate = useNavigate();
    const [error, setError] = useState(null);


    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (currentUser) => {
            setUser(currentUser);
        })

        return () => {
            unsubscribe();
        }
    }, [auth]);
    const handleLogin = async () => {
        try {
          // Verificați dacă emailul se găsește în colecția "deletes"
          const deletesCollection = collection(db, 'deletes');
          const q = query(deletesCollection, where('email', '==', email));
          const querySnapshot = await getDocs(q);
      
          if (querySnapshot.size === 0) {
            // Emailul nu există în colecția "deletes", așa că puteți continua cu autentificarea
            const userCredential = await signInWithEmailAndPassword(auth, email, password);
            const user = userCredential.user;
            console.log(user);
      
            if (user.email === 'cosmina.tunea@gmail.com') {
              navigate('/admin');
            } else {
              navigate('/home');
            }
          } else {
            // Emailul se găsește în "deletes", afișați un mesaj corespunzător
            setError('Firebase: Error (auth/invalid-login-credentials).');
          }
        } catch (error) {
          setError(error.message);
          console.log(error.message);
        }
      }
      
    return(
        <>
        <div className='wrraper1'>
        <h1 className='title'>Login</h1>
        <div>
        {error && <div style={{ color: 'red' }}>{error}</div>}
           <input 
                type='text' 
                className='place' 
                placeholder='Email'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
            />
           <input 
                type='password' 
                className='place' 
                placeholder='Password'
                value={password}
                onChange={(e) => setPassword(e.target.value)}
            />
        </div>    
        <div className='btn'>
            <button className='button' type='button' onClick={handleLogin}>Login</button>
        </div>
        <div>
            <p>Don&apos;t have an account?<Link to="/register">Register</Link></p>
        </div>
        <img src='src\images\cover.png'/>
        </div>
        </>
    )
}

export default Login