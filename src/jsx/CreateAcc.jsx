import { Link, useNavigate } from 'react-router-dom';
import '../css/CreateAcc.css';
import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth';
import { getFirestore, collection, addDoc, query, where, getDocs } from 'firebase/firestore';
import { useState } from 'react';

function CreateAcc() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [city, setCity] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const auth = getAuth();
  const db = getFirestore();

  const navigate = useNavigate();

  const handleSignUp = async () => {
    const userData = { firstName, lastName, email, phone, city };

    try {
      const userCredential = await createUserWithEmailAndPassword(auth, email, password);
      const user = userCredential.user;

      const emailQuery = query(collection(db, 'users'), where('email', '==', email));
      const emailSnapshot = await getDocs(emailQuery);

      if (!emailSnapshot.empty) {
        setError('Există deja un cont cu acest email');
        return;
      }

      await addDoc(collection(db, 'users'), {
        ...userData,
        uid: user.uid,
      });

      console.log('Datele au fost salvate cu succes');
      navigate('/home');
    } catch (error) {
      console.error('Eroare la înregistrare', error);
      setError(`A apărut o eroare la înregistrare: ${error.message}`);
    }
  };

  return (
    <>
      <div className='wrraper2'>
        <h1 className='title1'>Create Account</h1>
        {error && <div style={{ color: 'red' }}>{error}</div>}
        <form>
          <div>
            <input
              type='text'
              placeholder='First name'
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
            <input
              type='text'
              placeholder='Last name'
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
            <input
              type='email'
              placeholder='Email'
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <input
              type='text'
              placeholder='Phone'
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            />
            <input
              type='text'
              placeholder='City'
              value={city}
              onChange={(e) => setCity(e.target.value)}
            />
            <input
              type='password'
              placeholder='Password'
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="btn">
            <button className='button' type="button" onClick={handleSignUp}>SAVE</button>
          </div>
        </form>
        <div>
          <p>Do you have an account? <Link to="/login">Login</Link></p>
        </div>
        <img src='src\images\cover.png' />
      </div>
    </>
  );
}

export default CreateAcc;
