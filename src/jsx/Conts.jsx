import { useEffect, useState } from 'react';
import { collection, getDocs, deleteDoc, doc, updateDoc, addDoc, getDoc } from 'firebase/firestore';
import { db } from './firebase';
import { getAuth, createUserWithEmailAndPassword, deleteUser } from 'firebase/auth';
import { Link } from 'react-router-dom';

function Conts() {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [editData, setEditData] = useState({});
  const [newUserData, setNewUserData] = useState({
    firstName: '',
    lastName: '',
    city: '',
    email: '',
    phone: '',
    password: '', // Adăugați un câmp pentru parolă
  });
  const auth = getAuth(); // Inițializați autentificarea Firebase

  useEffect(() => {
    const loadUsers = async () => {
      try {
        const usersCollection = collection(db, 'users');
        const usersSnapshot = await getDocs(usersCollection);
        const usersData = usersSnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
        setUsers(usersData);
      } catch (error) {
        console.error('Eroare la încărcarea datelor:', error);
      }
    };

    loadUsers();
  }, []);
  const handleDeleteUser = async (userId) => {
    try {
      // Obțineți utilizatorul din Firestore
      const userRef = doc(db, 'users', userId);
      const userSnapshot = await getDoc(userRef);
  
      if (userSnapshot.exists()) {
        // Obțineți emailul utilizatorului
        const userEmail = userSnapshot.data().email;
  
        // Ștergeți utilizatorul din Firestore
        await deleteDoc(userRef);
  
        // Salvați emailul utilizatorului în colecția "deletes"
        const deletesCollection = collection(db, 'deletes');
        await addDoc(deletesCollection, { email: userEmail });
  
        // Ștergeți contul utilizatorului din Firebase Authentication
        await deleteUser(auth, userEmail);
  
        console.log('Utilizatorul a fost șters din Firestore, Authentication și salvat în colecția "deletes"');
      } else {
        console.log('Utilizatorul nu există sau nu a putut fi găsit în Firestore');
      }
    } catch (error) {
      console.error('Eroare la ștergerea utilizatorului:', error);
    }
  };
  
  
  
  

  const handleEditUser = (user) => {
    setSelectedUser(user.id);
    setEditData(user);
  };

  const handleAddUser = async () => {
    try {
      const usersCollection = collection(db, 'users');
      const newUserRef = await addDoc(usersCollection, newUserData);
      
      // Adăugați utilizatorul la Firebase Authentication cu aceeași adresă de email și parolă
      await createUserWithEmailAndPassword(auth, newUserData.email, newUserData.password);
      
      setNewUserData({
        firstName: '',
        lastName: '',
        city: '',
        email: '',
        phone: '',
        password: '', // Resetarea câmpului pentru parolă
      });
    } catch (error) {
      console.error('Error adding user:', error);
    }
  };

  const handleNewUserInputChange = (e) => {
    const { name, value } = e.target;
    setNewUserData({ ...newUserData, [name]: value });
  };

  const handleSaveEdit = async (userId) => {
    try {
      const userRef = doc(db, 'users', userId);
      await updateDoc(userRef, editData);
      setSelectedUser(null);
    } catch (error) {
      console.error('Eroare la actualizarea utilizatorului:', error);
    }
  }

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEditData({ ...editData, [name]: value });
  };

  return (
    <div>
      <button className='bt_a'>
        <Link to='/admin'>
          <p className='pp'>Administrator Page</p>
        </Link>
      </button>
      {/* Adăugați câmpuri pentru adăugarea unui nou utilizator, inclusiv parola */}
      <input
        type="text"
        name="firstName"
        placeholder="First Name"
        value={newUserData.firstName}
        onChange={handleNewUserInputChange}
      />
      <input
        type="text"
        name="lastName"
        placeholder="Last Name"
        value={newUserData.lastName}
        onChange={handleNewUserInputChange}
      />
      <input
        type="text"
        name="city"
        placeholder="City"
        value={newUserData.city}
        onChange={handleNewUserInputChange}
      />
      <input
        type="email"
        name="email"
        placeholder="Email"
        value={newUserData.email}
        onChange={handleNewUserInputChange}
      />
      <input
        type="password"
        name="password" // Câmpul pentru parolă
        placeholder="Password"
        value={newUserData.password}
        onChange={handleNewUserInputChange}
      />
      <input
        type="text"
        name="phone"
        placeholder="Phone"
        value={newUserData.phone}
        onChange={handleNewUserInputChange}
      />

      <button onClick={handleAddUser}>Add User</button>

      {users.map((user) => (
        <div key={user.id}>
          <h2>Utilizator: {user.firstName} {user.lastName}</h2>
          <p>Oraș: {user.city}</p>
          <p>Email: {user.email}</p>
          <p>Telefon: {user.phone}</p>
          {selectedUser === user.id ? (
            <>
              <input
                type="text"
                name="firstName"
                value={editData.firstName}
                onChange={handleInputChange}
              />
              <input
                type="text"
                name="lastName"
                value={editData.lastName}
                onChange={handleInputChange}
              />
              <input
                type="text"
                name="city"
                value={editData.city}
                onChange={handleInputChange}
              />
              <input
                type="text"
                name="email"
                value={editData.email}
                onChange={handleInputChange}
              />
              <input
                type="text"
                name="phone"
                value={editData.phone}
                onChange={handleInputChange}
              />
              <button onClick={() => handleSaveEdit(user.id)}>Salvează</button>
            </>
          ) : (
            <>
              <button onClick={() => handleEditUser(user)}>Editează</button>
              <button onClick={() => handleDeleteUser(user.id, user.email)}>Șterge</button>
            </>
          )}
        </div>
      ))}
    </div>
  );
}

export default Conts;
