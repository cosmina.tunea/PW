import { FaUser } from "react-icons/fa";
import '../css/Profile.css';
import { Link } from "react-router-dom";
import { getAuth, onAuthStateChanged, signOut } from "firebase/auth";
import { useEffect, useState } from "react";
import { addDoc, collection, query, where, onSnapshot, deleteDoc, doc, getDocs } from "firebase/firestore";
import { db } from './firebase';
import Post from './Post'; // Importă componenta Post

export default function Profile() {
  const auth = getAuth();
  const [user, setUser] = useState(null);
  const [inputText, setInputText] = useState('');
  const [userPosts, setUserPosts] = useState([]);
  const [notificationText, setNotificationText] = useState('');
  const [hasNotifications, setHasNotifications] = useState(false); 

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (currentUser) => {
      setUser(currentUser);
      if (currentUser) {
        const userId = currentUser.uid;
        const postsRef = collection(db, 'posts');
        const userPostsQuery = query(postsRef, where('userId', '==', userId));

        onSnapshot(userPostsQuery, (querySnapshot) => {
          const posts = [];
          querySnapshot.forEach((doc) => {
            posts.push({ id: doc.id, ...doc.data() });
          });
          setUserPosts(posts);
        });
        const notificationsRef = collection(db, 'Notifications');
        const userNotificationQuery = query(notificationsRef, where('userEmail', '==', currentUser.email));
        getDocs(userNotificationQuery)
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              setNotificationText(doc.data().text);
            });

            if (querySnapshot.size > 0) {
              setHasNotifications(true);
            } else {
              setHasNotifications(false);
            }
          })
          .catch((error) => {
            console.error("Error fetching notification: ", error);
          });
      } else {
        setUserPosts([]);
        setNotificationText('');
        setHasNotifications(false); // Asigurați-vă că starea hasNotifications este resetată când utilizatorul nu este autentificat
      }
    });

    return () => {
      unsubscribe();
    }
  }, [auth]);

  const logout = async () => {
    await signOut(auth);
  };

  const handleAdd = async (e) => {
    e.preventDefault();
    if (inputText.trim() === '') {
      return;
    }

    const userId = user.uid;
    const userEmail = user.email;

    const res = await addDoc(collection(db, 'posts'), {
      text: inputText,
      userEmail: userEmail,
      userId: userId
    });
    console.log(res);
    setInputText('');
  };

  const handleUpdatePost = () => {
    // Implementează logica pentru actualizarea anunțului
  };

  const handleDeletePost = async (postId) => {
    try {
      await deleteDoc(doc(db, 'posts', postId));
    } catch (error) {
      console.error("Error deleting document: ", error);
    }
  };
  


  const saveUserEmail = async () => {
    if (user) {
      try {
        const userEmail = user.email;
        const emailCollectionRef = collection(db, 'requests'); // Înlocuiți 'userEmails' cu numele colecției dorit
        await addDoc(emailCollectionRef, { email: userEmail });
        console.log('Email saved to requests collection.');
      } catch (error) {
        console.error('Error saving email to requests collection: ', error);
      }
    }
  };
  

  return (
    <div className="Container">
          
          {hasNotifications && (
      <div className="notif_c">
        <p className="notification">Notification from administrator: {notificationText}</p>
      </div>
    )}
    {hasNotifications && (
      <button className="bt_r" onClick={saveUserEmail}>Request deletion of advertisement</button>
    )}
      <div className="left-section">
        <FaUser className="U_icon" />
        <div>
          <p>Email: {user ? user.email : '...'} </p>
        </div>
      </div>
      <div className="anunt">
        <p>Write something about yourself: </p>
        <textarea
          type="text"
          className="anuntI"
          placeholder="Text + email/phone"
          value={inputText}
          onChange={(e) => setInputText(e.target.value)}
        ></textarea>
        <button className="button" onClick={handleAdd}>Post</button>
      </div>
      <div className="command">
        <button className="btt button"><Link to="/home">Home</Link></button>
        <button className="btt button" onClick={logout}><Link to="/login">Logout</Link></button>
      </div>
      <div className="at">
        <p>My posts: </p>
        <div>
          {userPosts.map((post) => (
            <Post
              key={post.id}
              post={post}
              handleUpdate={handleUpdatePost}
              handleDelete={handleDeletePost}
            />
          ))}
        </div>
      </div>
    </div>
  );
}
