import { useState } from "react";
import { db } from './firebase';
import PropTypes from 'prop-types';
import { doc, updateDoc } from "firebase/firestore"; 

function Post({ post, handleUpdate, handleDelete }) {
  const [isEditing, setIsEditing] = useState(false);
  const [updatedText, setUpdatedText] = useState(post.text);

  const handleEdit = () => {
    setIsEditing(true);
  };

  const handleSave = async () => {
    if (updatedText.trim() === "") {
      return; // Nu salvăm text gol
    }

    // Apelați funcția handleUpdate pentru a actualiza textul anunțului în componenta părinte
    handleUpdate(post.id, updatedText);

    // Actualizați și în baza de date
    const postRef = doc(db, "posts", post.id);
    await updateDoc(postRef, {
      text: updatedText,
    });

    setIsEditing(false);
  };

  const handleCancel = () => {
    setIsEditing(false);
    setUpdatedText(post.text);
  };

  const splitText = (text) => {
    const maxLength = 50;
    if (text.length <= maxLength) {
      return text;
    }

    const firstPart = text.slice(0, maxLength);
    const secondPart = text.slice(maxLength);

    return (
      <>
        {firstPart}
        <br />
        {splitText(secondPart)} {/* Recursiv pentru a trata textul mai lung */}
      </>
    );
  };

  return (
    <div className="post-container">
      {isEditing ? (
        <input
          type="text"
          value={updatedText}
          onChange={(e) => setUpdatedText(e.target.value)}
        />
      ) : (
        <p className="post-text">{splitText(updatedText)}</p>
      )}
      <div className="button-container">
        {isEditing ? (
          <>
            <button onClick={handleSave}>Save</button>
            <button onClick={handleCancel}>Cancel</button>
          </>
        ) : (
          <>
            <button onClick={handleEdit}>Update</button>
            <button onClick={() => handleDelete(post.id)}>Delete</button>
          </>
        )}
      </div>
    </div>
  );
}

Post.propTypes = {
  post: PropTypes.shape({
    text: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired, // Verificați dacă 'id' este o proprietate necesară
  }),
  handleUpdate: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
};

export default Post;
