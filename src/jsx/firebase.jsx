// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore} from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBPhKM1v7f8d7MSRg_EwegcuAZFvbgViIk",
  authDomain: "project-pw-bae82.firebaseapp.com",
  projectId: "project-pw-bae82",
  storageBucket: "project-pw-bae82.appspot.com",
  messagingSenderId: "804541143993",
  appId: "1:804541143993:web:89995d3d139f07b8367468",
  measurementId: "G-KH226ZJJER",
  databaseURL: "https://DATABASE_NAME.firebaseio.com",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Firebase Authentication and get a reference to the service
export const db = getFirestore(app);
export const auth = getAuth(app);
