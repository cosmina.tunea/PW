import { useEffect, useState } from 'react';
import { collection, getDocs, addDoc, query, where } from 'firebase/firestore';
import { db } from './firebase';
import { Link } from 'react-router-dom';
import { getAuth, signOut } from 'firebase/auth';
import UserNotifications from './UserNotifications';
import '../css/AdminPage.css';

function AdminPage() {
  const [userPosts, setUserPosts] = useState([]);
  const auth = getAuth();
  const [emailStatus, setEmailStatus] = useState([]);
  

  useEffect(() => {
    const fetchUserPosts = async () => {
      const postsCollection = collection(db, 'posts');
      const querySnapshot = await getDocs(postsCollection);

      const postsByUser = {};

      querySnapshot.forEach((doc) => {
        const postData = doc.data();
        const userId = postData.userId;

        if (!postsByUser[userId]) {
          postsByUser[userId] = {
            userEmail: postData.userEmail,
            posts: [],
          };
        }

        postsByUser[userId].posts.push({
          id: doc.id,
          text: postData.text,
        });
      });

      setUserPosts(Object.values(postsByUser));

      const emailStatusObject = {};

      for (const user of Object.values(postsByUser)) {
        const isRequested = await isEmailRequested(user.userEmail);
        emailStatusObject[user.userEmail] = isRequested;
      }
  
      setEmailStatus(emailStatusObject);
    };

    fetchUserPosts();
  }, []);

  const checkNotification = async (userEmail) => {
    const notifCollection = collection(db, 'Notifications');
    const querySnapshot = await getDocs(notifCollection);
    return querySnapshot.docs.some((doc) => doc.data().userEmail === userEmail);
  };

  const handleSendNotif = async (userEmail) => {
    const notificationExists = await checkNotification(userEmail);

    if (!notificationExists) {
      const text = prompt('Enter notification text:');
      if (text) {
        try {
          const notifData = {
            text: text,
            userEmail: userEmail,
          };

          const notifCollection = collection(db, 'Notifications');
          await addDoc(notifCollection, notifData);
          alert('Notification sent successfully!');
        } catch (error) {
          console.error('Error sending notification', error);
          alert('There was an error sending the notification.');
        }
      }
    } else {
      alert('You have already sent a notification to this user.');
    }
  };


  const logout = async () => {
    await signOut(auth);
  };


  const isEmailRequested = async (userEmail) => {
    const requestsRef = collection(db, 'requests');
    const requestQuery = query(requestsRef, where('email', '==', userEmail));
    const querySnapshot = await getDocs(requestQuery);
  
    console.log('Query Snapshot:', querySnapshot.docs.length); // Afișați numărul de documente găsite în querySnapshot.
  
    return !querySnapshot.empty;
  };

  return (
    <div className='wrraper'>
      <h1 className='h1'>ADMINISTRATOR</h1>
      <button className="b button" onClick={logout}><Link to="/login">Logout</Link></button>
      <button ><Link to="/conts">Administrare conturi</Link></button>
      <div className='allPosts'>
        <h2 className='post_text'>All Posts</h2>
        {userPosts.map((user) => (
          <div key={user.userEmail} className='post-container'>
            <h3 className={emailStatus[user.userEmail] ? "red-email" : "post_text"}>
              {user.userEmail}:
            </h3>
            <ul>
            {user.posts.map((post, index) => (
                <li key={post.id} className='post-container2 pst'>
                  <p className='post_text'>{index + 1}. {post.text}</p>
                </li>
              ))}
            </ul>
            <button className='btn_n' onClick={() => handleSendNotif(user.userEmail)}>Send advertisment</button>
            <UserNotifications userEmail={user.userEmail} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default AdminPage;
