import "../css/Start.css"
import { IoIosArrowForward } from 'react-icons/io';
import { Link } from 'react-router-dom';


export default function Start(){  
    return(
        <>
        <div>
            <img src="src\images\profile.png"></img>
        </div>
        <p className="slog">Join the Fun with Party Pals: Friends Just a Click Away!</p>
        <div className="container">
        <div className="cnt">
            <img src="src\images\orbit.png" className="world"/>
        </div>
        <div className="icon_c">
            <img src="src\images\smiley.png" className="emoji"/>
        </div>
        </div>
        <div>
            <Link to="/register">
            <IoIosArrowForward className="icon"/>
            </Link>
        </div>
        </>
    )
}
