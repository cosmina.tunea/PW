import { Link } from 'react-router-dom';
import '../css/Home.css';
import { CiUser } from 'react-icons/ci';
import { db } from './firebase';
import { useEffect, useState } from 'react';
import { collection, getDocs } from 'firebase/firestore';


function Home() {
  const [allPosts, setAllPosts] = useState([]);

  useEffect(() => {
    const fetchAllPosts = async () => {
      const postsCollection = collection(db, 'posts');
      const querySnapshot = await getDocs(postsCollection);

      const posts = [];
      querySnapshot.forEach((doc) => {
        posts.push({ id: doc.id, ...doc.data() });
      });
      setAllPosts(posts);
    }
    fetchAllPosts();
  }, []);

  // Definește funcția splitText pentru a împărți textul în componenta Home
  const splitText = (text, maxLength) => {
    if (text.length <= maxLength) {
      return text;
    }

    const firstPart = text.slice(0, maxLength);
    const secondPart = text.slice(maxLength);

    return (
      <>
        {firstPart}
        <br />
        {splitText(secondPart, maxLength)} {/* Recursiv pentru a trata textul mai lung */}
      </>
    );
  };

  return (
    <div className='wrraper'>
      <h1 className='h'>Home Page</h1>
      <div className='userTag'>
        <button className='userTag button'>
          <Link to="/profile"><CiUser className='user' /></Link>
        </button>
        <div className='btn_t'>
        <button className='b_t'>
        <Link to="/termeni"><p className='termeni'>Terms & conditions</p></Link> 
        </button>
        </div>
        </div>
        
      <div className='allPosts'>
        <h2 className='post_text'>All Posts</h2>
        {allPosts.map((post) => (
          <div key={post.id} className='post-container'>
            <p className='post_text email'>
              Email: {post.userEmail}
            </p>
            <p className='post_text'>
              {splitText(post.text, 100)}
            </p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Home;
