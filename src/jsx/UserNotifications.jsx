// UserNotifications.js
import { useEffect, useState } from 'react';
import { collection, query, where, getDocs, doc, updateDoc, deleteDoc } from 'firebase/firestore';
import { db } from './firebase';
import PropTypes from 'prop-types'; // Import prop-types library

function UserNotifications({ userEmail }) {
  const [userNotifs, setUserNotifs] = useState([]);
  const [editingNotificationId, setEditingNotificationId] = useState(null);
  const [editedText, setEditedText] = useState('');

  const fetchUserNotifs = async () => {
    const notifRef = collection(db, 'Notifications');
    const notifQuery = query(notifRef, where('userEmail', '==', userEmail));
    const notifSnapshot = await getDocs(notifQuery);
    const notifs = [];
    notifSnapshot.forEach((doc) => {
      const notifData = doc.data();
      notifs.push({
        id: doc.id,
        text: notifData.text,
      });
    });
    setUserNotifs(notifs);
  };

  useEffect(() => {
    fetchUserNotifs();
  }, [userEmail]);

  const handleEdit = (notificationId, text) => {
    setEditingNotificationId(notificationId);
    setEditedText(text);
  };

  const handleUpdate = async (notificationId) => {
    if (editedText.trim() === '') {
      alert('Enter valid notification text.');
      return;
    }

    try {
      const notifDocRef = doc(db, 'Notifications', notificationId);
      await updateDoc(notifDocRef, { text: editedText });
      setEditingNotificationId(null);
      setEditedText('');
      // Reîmprospătarea notificărilor după actualizare
      fetchUserNotifs();
    } catch (error) {
      console.error('Error updating notification', error);
      alert('There was an error updating the notification.');
    }
  };

  const handleCancelEdit = () => {
    setEditingNotificationId(null);
    setEditedText('');
  };

  const handleDelete = async (notificationId) => {
    if (window.confirm('You&apos;re sure you want to delete this notification?')) {
      try {
        const notifDocRef = doc(db, 'Notifications', notificationId);
        await deleteDoc(notifDocRef);
        // Reîmprospătarea notificărilor după ștergere
        fetchUserNotifs();
      } catch (error) {
        console.error('Error deleting notification', error);
        //alert('A apărut o eroare la ștergerea notificării.');
      }
    }
  };

  const handleDeleteNotif = async (userEmail) => {
    if (window.confirm('Are you sure you want to delete this notification?')) {
      try {
        // Șterge notificarea din colecția "Notifications"
        // (așa cum ai deja implementat în funcția handleDeleteNotif din UserNotifications.js)
  
        // Șterge email-ul din colecția "requests"
        const requestsCollection = collection(db, 'requests');
        const userQuery = query(requestsCollection, where('email', '==', userEmail));
        const userQuerySnapshot = await getDocs(userQuery);
  
        if (!userQuerySnapshot.empty) {
          userQuerySnapshot.docs.forEach(async (doc) => {
            await deleteDoc(doc.ref);
          });
        }
      } catch (error) {
        console.error('Error deleting notification and email from requests', error);
        // Poți trata eroarea în modul pe care-l dorești
      }
    }
  };
  

  return (
    <div className="user-notifications">
      <h3>Notification send to {userEmail}:</h3>
      <ul>
        {userNotifs.map((notif) => (
          
          <li key={notif.id}>
            {editingNotificationId === notif.id ? (
              <>
                <input
                  type="text"
                  value={editedText}
                  onChange={(e) => setEditedText(e.target.value)}
                  placeholder=""
                />
                <button className='btn_b' onClick={() => handleUpdate(notif.id)}>Save</button>
                <button className='btn_b' onClick={handleCancelEdit}>Cancel</button>
              </>
            ) : (
              <div><p className='notif_txt'>{notif.text}</p></div>
            )}
            <button className='btn_b' onClick={() => handleEdit(notif.id, notif.text)}>Update</button>
            <button className='btn_b' onClick={() => handleDelete(notif.id) && handleDeleteNotif(userEmail)}>Delete</button>
          </li>
        ))}
      </ul>
    </div> 
  );
}

// Adăugare validare prop tip
UserNotifications.propTypes = {
  userEmail: PropTypes.string.isRequired,
};

export default UserNotifications;
