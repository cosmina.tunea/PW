
import { Link } from 'react-router-dom';
import '../css/Termeni.css';

function Termeni(){
    return(
        <>
        <h1>Terms and Conditions</h1>
        <button className='bb'><Link to="/home">Home</Link></button>
        <div>
            <ol>
                <li>
                <h4>Scopul Paginii:</h4> Această pagină este destinată exclusiv găsirii prietenilor și interacțiunii sociale. Nu este permisă vânzarea sau promovarea de bunuri sau servicii comerciale.
                </li>
                <li>
                <h4>Publicarea Anunțurilor:</h4> Utilizatorii pot publica anunțuri care să descrie în mod adecvat scopul lor de a căuta prieteni sau de a lega relații sociale. Anunțurile nu trebuie să conțină informații ofensatoare, mincinoase sau care ar putea încuraja comportamentul ilegal. Nu este permisă comercializarea de produse online.
                </li>
                <li>
                <h4>Confidențialitate:</h4> Administrarea paginii nu va partaja informațiile personale ale utilizatorilor fără consimțământul lor și va lua măsuri pentru a proteja datele personale ale utilizatorilor.
                </li>
                <li>
                    <h4>Suspendarea anunțurilor:</h4>Administrarea își rezervă dreptul de a trimite advertismente celor care încalcă regulile aplicației, dacă utilizatorii care au primit advertisment de la administrare nu își modifică conținutul anunțurilor vor fi blocați.
                </li>
            </ol>
        </div>
        </>
    )
}

export default Termeni;